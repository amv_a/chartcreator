const express = require('express');
const app = express();
const path = require('path');
const mongoose = require('mongoose');

// connection to the MongoDB database.
mongoose.connect('mongodb+srv://root:HKkfX5SDvMYwDoTx'+
  '@chartapp-hq1au.gcp.mongodb.net/chartDatabase'+
  '?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
mongoose.connection.on('error', (error) => {
  console.log('ERROR' + error);
});
mongoose.connection.once('open', () => {
  console.log('Succesfully connected to the database');
});
// parsing the information comming from the web browse.
app.use(express.json());
// static web server
app.use(express.static(path.join(__dirname, 'public')));

// routers
app.use('/api/savechart', require('./routes/save.js'));
app.use('/api/readchart', require('./routes/read.js'));

app.listen(3000, () => {
  console.log('Listening at port localhost:3000');
});
