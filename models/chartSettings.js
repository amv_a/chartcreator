const mongoose = require('mongoose');
const chartSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  labels: [String],
  colors: [String],
  numbers: [Number],
});

/**
 * The first parameter must be the singular of the
 * name of the desired collection, like this:
 * chartSettings becomes -> chartSetting.
 */
module.exports = mongoose.model('chartSetting', chartSchema);
