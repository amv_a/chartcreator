const express = require('express');
const router = express.Router();
const ChartModel = require('../models/chartSettings');

// routes.
router.post('/', (request, response) => {
  // this saves the info comming from the browser.
  const input = request.body;
  const newDocument = new ChartModel({
    title: input.title,
    description: input.description,
    labels: input.labels,
    colors: input.colors,
    numbers: input.numbers,
  });
  newDocument.save((error)=> {
    if (error) {
      console.log('ERROR: ' + error);
      response.status(500).json({
        message: 'Problems while saving the data',
        success: false,
      });
    } else {
      console.log('Data successfully saved');
      response.status(200).json({
        message: 'Data successfully saved',
        success: true,
      });
    }
  });
});

module.exports = router;
