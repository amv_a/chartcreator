const express = require('express');
const router = express.Router();
const chartModel = require('../models/chartSettings');
// routes.
/**
 * The first one is for getting all the data inside the
 * database.
 */
router.get('/all', (request, response) => {
  // this will find the data, docs contains all the info.
  chartModel.find((error, docs) => {
    if (error) {
      console.log('ERROR: ' + error);
      response.status(500).json({
        message: 'An error ocurred while reading from the database',
        success: false,
      });
    } else {
      console.log('Successfully read the information from the database');
      response.status(200).json({
        documents: docs,
        success: true,
      });
    }
  });
});
/**
 * The second one is for getting the desired object with
 * the id given.
 */
router.get('/:chartId', (request, response) => {
  chartModel.findOne({
    _id: request.params.chartId,
  }, (error, doc) => {
    if (error) {
      console.log('ERROR: ' + error);
      response.status(500).json({
        message: 'An error ocurred while reading from the database',
        success: false,
      });
    } else {
      console.log('Successfully read the information from the database');
      response.status(200).json({
        documents: doc,
        success: true,
      });
    }
  });
});

module.exports = router;
